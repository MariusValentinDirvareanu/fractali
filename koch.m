
function koch(n) %n numarul de iteratii.
    if (n==0)
        x=[0;1];
        y=[0;0];
        line(x,y,'Color','c');
        title(['Curba lui koch pentru ' num2str(n) ' iteratii'],'Color','c');
        axis equal
        axis off
    else
        L=(4/3)^n;
        lim=ceil(L);
        fkoch(0,0,4^n,0,lim);
        axis equal
        axis off
        if(n==1)
            title(['Curba lui koch pentru o iteratie'],'Color','r');
        else
             title(['Curba lui koch pentru ' num2str(n) ' iteratii'],'Color','b');
        end
        set(gcf,'Name','Curba koch')
    end

    function fkoch(x1,y1,x5,y5,lim)   
        l=sqrt((x5-x1)^2+(y5-y1)^2);
        
        if(l>lim)
            x2=(2*x1+x5)/3;
            y2=(2*y1+y5)/3;
            x3=(x1+x5)/2-(y5-y1)/(2*sqrt(3));
            y3=(y1+y5)/2+(x5-x1)/(2*sqrt(3));
            x4=(2*x5+x1)/3;
            y4=(2*y5+y1)/3;

            fkoch(x1,y1,x2,y2,lim);
            fkoch(x2,y2,x3,y3,lim);
            fkoch(x3,y3,x4,y4,lim);
            fkoch(x4,y4,x5,y5,lim);
        else 
            frac(x1,y1,x5,y5); 
        end

        function frac(a1,b1,a2,b2)
            x=[a1;a2];
            y=[b1;b2];
              if(n==1)
                  line(x,y,'Color','r');
              else
                  line(x,y,'Color','b');
              end
        end

        end
end