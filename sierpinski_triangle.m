
function sierpinski_triangle(it)
  
%dimensiunea de autosimilaritate log(3)/log(2)=1.585
    L = 10; %latura triunghiului.
    it = it - 1;

    % Initializarea triunghiului.
    sop_x = [0 L/2 L];
    sop_y = [0 sqrt(3*(L^2)/4) 0];
    fill(sop_x, sop_y, 'k'); hold on; axis square; ylim([0 L]);
    title(['Triunghiul lui Sierpinski  pentru ' num2str(it+1) ' iteratii']);
    clear('sop_x','sop_y');
    
    % Iteratia 0
    L = L/2;
    it0_x = [L/2 L 3*L/2];
    it0_y = [sqrt(3*(L^2)/4) 0 sqrt(3*(L^2)/4)];
    fill(it0_x, it0_y, 'w'); hold on;

    % matricea care va contine coordonatele triunghiului
    M{1,1} = it0_x; 
    M{2,1} = it0_y; 

     % Genereaza iteratiile.
        for i = 1:1:it
            temp = {};
            L = L/2;
            for n = 1:1:size(M,2)
                
                h = sqrt(3*(L^2)/4);

                % triunghiul din dreapta
                x_r = [M{1,n}(2)+L/2 M{1,n}(2)+L M{1,n}(2)+3*L/2];
                y_r = [M{2,n}(2)+h M{2,n}(2) M{2,n}(2)+h];
                fill(x_r, y_r, 'w'); hold on;

                % triunghiul de sus
                x_u = [M{1,n}(1)+L/2 M{1,n}(1)+L M{1,n}(1)+3*L/2];
                y_u = [M{2,n}(1)+h M{2,n}(1) M{2,n}(1)+h];
                fill(x_u, y_u, 'w'); hold on;

                % triunghiul din stanga.
                x_l = [M{1,n}(2)-3*L/2 M{1,n}(2)-L M{1,n}(2)-L/2];
                y_l = [M{2,n}(2)+h M{2,n}(2) M{2,n}(2)+h];
                fill(x_l, y_l, 'w'); hold on;

                % celule dinamice in care se stocheaza coordonatele
                % triunghiului pentru a putea fi folosite la urmatoarele
                % iteratii
                l = size(temp,2);
                temp{1,l+1} = x_r; temp{2,l+1} = y_r;
                temp{1,l+2} = x_u; temp{2,l+2} = y_u;
                temp{1,l+3} = x_l; temp{2,l+3} = y_l;
            end
            % stergerea datelor temporale.
            M = temp;
            clear('temp');
        end
        hold off;
    
    axis off;
end