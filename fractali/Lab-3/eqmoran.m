% s1=1/2;s2=1/4;
% syms D;
% eqn=s1^D+s2^D==1;
% sol=solve(eqn,D)
% sol=double(sol)
% if(sol(1)>0)
%     s=sol(1)
% else s=sol(2)
% end
% 
% s1=1/2;s2=1/2;s3=1/2;s4=1/4;
% syms D;
% eqn=s1^D+s2^D+s3^D+s4^D==1;
% sol=solve(eqn,D)
% sol=double(sol)
% if(sol(1)>0)
%     s=sol(1)
% else s=sol(2)
% end
% 
% s1=1/3;s2=1/3;s3=1/3;s4=2/3;
% syms D;
% eqn=s1^D+s2^D+s3^D+s4^D==1;
% sol=solve(eqn,D)
% sol=double(sol)
% if(sol(1)>0)
%     s=sol(1)
% else s=sol(2)
% end

% s1=1/4;s2=1/4;s3=1/4;s4=2/4;s5=1/2;
% syms D;
% eqn=s1^D+s2^D+s3^D+s4^D+s5^D==1;
% sol=solve(eqn,D)
% sol=double(sol)
% if(sol(1)>0)
%     s=sol(1)
% else s=sol(2)
% end

N=input('numar de copii: ')
s=zeros(N);
syms D;
f=0
for i=1:N
 s(i)=input(strcat('s(',num2str(i),')= '));
 f= f+(s(i))^D
end
sol=solve(f==1,D)
sol=double(sol)
if(sol(1)>0)
    s=sol(1)
else s=sol(2)
end
