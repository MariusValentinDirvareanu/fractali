c = imread('fractal.jpg');
image(c)
axis image

i = c(:,:,3);
bi = (i<80);
imagesc(bi)
colormap gray
axis image

[n,r] = boxcount(bi,'slope');

df = -diff(log(n))./diff(log(r));
disp(['Fractal dimension, Df = ' num2str(mean(df(4:8))) ' +/- ' num2str(std(df(4:8)))]);
