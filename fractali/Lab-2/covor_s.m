function covor_s(n)
a= zeros(10,10);

if(n<1) 
    figure;subplot(1,1,1);imshow(a);
else 
    for i=1:n
    c=ones(size(a)*3);
    c((1:size(c,1)/3),(1:size(c,1)/3))=a;
    c((1:size(c,1)/3),(size(c,1)/3)+1:2*size(c,1)/3)=a;
    c((1:size(c,1)/3),(2*size(c,1)/3+1):size(c,1))=a;
    c((size(c,1)/3)+1:2*size(c,1)/3,(1:size(c,1)/3))=a;
    
    c((size(c,1)/3)+1:2*size(c,1)/3,(2*size(c,1)/3+1):size(c,1))=a;
    c((2*size(c,1)/3+1):size(c,1),(1:size(c,1)/3))=a;
    c((2*size(c,1)/3+1):size(c,1),(size(c,1)/3)+1:2*size(c,1)/3)=a;
    c((2*size(c,1)/3+1):size(c,1),(2*size(c,1)/3+1):size(c,1))=a;
    
    a=c;
    end;
    figure;
    subplot(1,1,1);imshow(a);
end; 