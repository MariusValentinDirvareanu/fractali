function sierpinskih(n)
a= ones(10,10);
for i=1:10
    for j=1:i
        a(i,j)=0;
    end;
end;
if(n<1) 
    figure;subplot(1,1,1);imshow(a);
else 
    for i=1:n
    c=ones(size(a)*2);
    c((1:size(c,1)/2),(1:size(c,1)/2))=a;
    c((size(c,1)/2+1):size(c,1),1:(size(c,1)/2))=a;
    c(((size(c,1)/2)+1):size(c,1),(size(c,1)/2+1):size(c,1))=a;
    
    a=c;
    end;
    figure;
    subplot(1,1,1);imshow(a);
end; 