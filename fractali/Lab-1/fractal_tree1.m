%To run this file in the command window, call the function fractal_tree1 
%and input the appropriate arguments. This function takes input arguments 
%of staring enpoint and final endpoint of the first branch and the number 
%levels you wish to be iin your tree.
function recursions = fractal_tree1(x1, y1, x2, y2,r)
theta = pi/2; 
endpoint1 = [x1; y1];
recursions = zeros(2);
plot([x1 x2],[y1 y2]) %Draws the initial line that will be the "trunk". 
hold on
      if r > 0
          draw_left_branch(x1,y1,x2,y2);
          draw_right_branch(x1,y1,x2,y2);        
      end
hold off 
end
function left_branches = draw_left_branch(x1,y1,x2,y2)
    theta = pi/2;
    R_l = [cos(-theta) sin(-theta);-sin(-theta) cos(-theta)];
    v_12 = [x2-x1;y2-y1];
    v_rotated_l = (R_l*v_12)*0.6;
    endpoint2 = [x2; y2];
    endpointnew_l = v_rotated_l+endpoint2;
    x1 = endpointnew_l(1);
    y1 = endpointnew_l(2);
    plot([x2 x1],[y2 y1])
end
function right_branches = draw_right_branch(x1,y1,x2,y2) 
    theta = pi/2; 
    R_r = [cos(theta) sin(theta);-sin(theta) cos(theta)]; 
    v_12 = [x2-x1;y2-y1]; 
    v_rotated_r = (R_r*v_12)*0.6; 
    endpoint2 = [x2; y2];
    endpointnew_r = v_rotated_r+endpoint2; 
    x1 = endpointnew_r(1); 
    y1 = endpointnew_r(2); 
    plot([x2 x1],[y2 y1]) 
end