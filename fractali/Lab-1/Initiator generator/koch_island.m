function []=koch_island(n)
theta=60*pi/180;
p1=[0 0];
p2=[1 0];
p3=[1/2,sin(theta)];

vec=[transpose(p1) transpose(p2) transpose(p3)];

for i= 1:n
    c=vec(:,1);
    for j=1:size(vec,2)
     c=cat(2,c,vec(:,i+1)/(i*3))   
    end
end
figure
plot(vec)
plot(transpose(p1),transpose(p2))
figure
line([0 1],[0 0])
line([1 0.5],[0 0.8660])
line([0.5 0],[0.8660 0])
vec=cat(2,vec,transpose(p2))