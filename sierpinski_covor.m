function sierpinski_covor(i)
M = 0;
%dimensiunea de autosimilarite log(8)/log(3)=1.8928
for k=1:i
    %matrice de 0 si 1 in care sunt indicate pozitiile "covorului"
    M = [M, M, M; M, ones(3^(k-1)), M; M,M,M];
end

imagesc(M)
colormap(gray)

axis equal;
axis off;
